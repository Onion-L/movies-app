import React from "react";
import { Route, Navigate, Routes } from "react-router-dom";

const RouterView = ({ routes }) => {
  return (
    <Routes>
      {routes.map((route, index) => {
        return (
          <Route
            key={index}
            path={route.path}
            element={
              route.redirect ? (
                <Navigate to={route.redirect} />
              ) : (
                <route.component />
              )
            }
          />
        );
      })}
    </Routes>
  );
};

export default RouterView;
