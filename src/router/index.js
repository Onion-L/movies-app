import HomePage from "../pages/homePage";
import PersonPage from "../pages/personPage";
import SearchPage from "../pages/searchPage";
import FavoriteMoviesPage from "../pages/favoriteMoviesPage";
import UpcomingMoviesPage from "../pages/upcomingMoviesPage";
import TrendMoviesPage from "../pages/trendMoviesPage";
import AddMovieReviewPage from "../pages/addMovieReviewPage";
import MovieReviewPage from "../pages/movieReviewPage";
import MoviePage from "../pages/movieDetailsPage";
import PersonDetailPage from "../pages/personDetailPage";
import LoginPage from "../pages/loginPage";
import LatestMoviesPage from "../pages/latestMoviesPage";

const routes = [
  {
    path: "/",
    component: HomePage,
    redirect: "/home",
  },
  {
    path: "/home",
    component: HomePage,
  },
  {
    path: "/login",
    component: LoginPage,
  },
  {
    path: "/people",
    component: PersonPage,
  },
  {
    path: "/people/:id",
    component: PersonDetailPage,
  },
  {
    path: "/search",
    component: SearchPage,
  },
  {
    path: "/movies/now-playing",
    component: LatestMoviesPage,
  },
  {
    path: "/movies/favorites",
    component: FavoriteMoviesPage,
  },
  {
    path: "/movies/upcoming",
    component: UpcomingMoviesPage,
  },
  {
    path: "/movies/trend",
    component: TrendMoviesPage,
  },
  {
    path: "/reviews/form",
    component: AddMovieReviewPage,
  },
  {
    path: "/reviews/:id",
    component: MovieReviewPage,
  },
  {
    path: "/movies/:id",
    component: MoviePage,
  },
  {
    path: "*",
    redirect: "/",
  },
];

export default routes;
