// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCcnYRY5ktZdFx07UKlPXGN0fi7kFcKWTM",
  authDomain: "react-movies-app-3fef5.firebaseapp.com",
  projectId: "react-movies-app-3fef5",
  storageBucket: "react-movies-app-3fef5.appspot.com",
  messagingSenderId: "137096626495",
  appId: "1:137096626495:web:c811bbbe4ca8ec2b2d138a",
  measurementId: "G-F7ZW82XTR6",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
