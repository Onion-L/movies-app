import React, { useEffect, useContext } from "react";
import { useQuery } from "react-query";
import { MoviesContext } from "../contexts/moviesContext";
import PageTemplate from "../components/templateMovieListPage";
import { getLatestMovies } from "../api/tmdb-api";
import Spinner from "../components/spinner";

const LatestMoviesPage = () => {
  const { page, changePage } = useContext(MoviesContext);

  useEffect(() => {
    changePage(1);
  }, []);

  const handleChange = (event, value) => {
    changePage(value);
  };

  const { data, error, isLoading, isError } = useQuery(
    ["latest", { page }],
    getLatestMovies
  );

  if (isLoading) {
    return <Spinner />;
  }

  if (isError) {
    return <h1>{error.message}</h1>;
  }

  const latestMovies = data.results;

  return (
    <PageTemplate
      title="Upcoming Movies"
      movies={latestMovies}
      changePage={handleChange}
      page={page}
      action={() => {
        return null;
      }}
    />
  );
};

export default LatestMoviesPage;
