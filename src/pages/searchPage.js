import React, { useState, useEffect } from "react";
import { getMovies } from "../api/tmdb-api";
import PageTemplate from "../components/templateMovieListPage";
import AddToFavoritesIcon from "../components/cardIcons/addToFavorites";
import Spinner from "../components/spinner";
import { useQuery } from "react-query";
import { useLocation } from "react-router-dom";

const SearchPage = () => {
  let location = useLocation();
  let param = location.search;
  const [page, setPage] = useState(1);
  let index = 1;

  const { data, error, isLoading, isError } = useQuery(
    ["discover", { page }],
    getMovies
  );
  if (isLoading) {
    return <Spinner />;
  }

  if (isError) {
    return <h1>{error.message}</h1>;
  }

  let movies = data.results;
  let searchWord = param.split("=")[1];
  let displayedMovies = movies.filter((m) => {
    return m.title.toLowerCase().search(searchWord.toLowerCase()) !== -1;
  });

  console.log("123", data);
  console.log(param.split("=")[1]);
  console.log(displayedMovies);
  if (displayedMovies.length !== 0) {
    return (
      <PageTemplate
        movies={displayedMovies}
        action={(movie) => {
          return <AddToFavoritesIcon movie={movie} />;
        }}
      />
    );
  } else {
    setPage(2);
  }
};

export default SearchPage;
